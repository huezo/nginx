#FROM ubuntu:focal
FROM ubuntu:focal

#https://hub.docker.com/_/ubuntu/


#ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_FRONTEND noninteractive


LABEL maintainer="Huezo <huezohuezo.1990@gmail.com>"

WORKDIR /

# Install NGINX Plus


RUN mkdir -p /etc/ssl/nginx/
COPY ./nginx-repo.crt /etc/ssl/nginx/
COPY ./nginx-repo.key /etc/ssl/nginx/

RUN apt update 
COPY ./nginx_signing.key /nginx_signing.key
RUN apt-get install apt-transport-https lsb-release ca-certificates gnupg2 -y
RUN apt-key add nginx_signing.key

RUN echo "deb https://plus-pkgs.nginx.com/ubuntu focal nginx-plus" >>  /etc/apt/sources.list.d/nginx-plus.list
RUN apt install wget -y 
COPY ./90nginx /etc/apt/apt.conf.d/90nginx 
RUN apt update
RUN apt install nginx-plus  aptitude iputils-ping curl wget nano links elinks w3m w3m-img apt-utils -y 
RUN apt install nginx-plus-module-geoip nginx-sync nginx-ha-keepalived -y 
RUN apt install  nginx-sync nginx-ha-keepalived -y
RUN apt-get install -f -y
#RUN aptitude install  -y nginx-plus-* 

# default.conf

COPY ./default.conf /etc/nginx/conf.d/default.conf

#dashboard

COPY ./dashboard.conf /etc/nginx/conf.d/dashboard.conf

#VOLUME
VOLUME /usr/share/nginx/html
VOLUME /etc/nginx
VOLUME /var/log/nginx/log


# Reenviar registros de solicitudes al recolector de registros de Docker

RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log

#RUN ln -sf /dev/stdout /var/log/nginx/access.log   && ln -sf /dev/stderr /var/log/nginx/error.log


STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]

#  nginx

EXPOSE 80
EXPOSE 443

# nginx dashboard

EXPOSE 8080



